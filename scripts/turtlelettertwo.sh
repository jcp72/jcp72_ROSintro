#!/usr/bin/bash
rosservice call /reset
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[0.0 , -4.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[0.0 , -4.0 , 0.0]' '[0.0 , 0.0 , -3.14]'
rosservice call /turtle1/set_pen "{r: 0, g: 0, b: 0, width: 0}" 
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[-4.0 , 0.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rosservice call /turtle1/set_pen "{r: 100, g: 255, b: 100, width: 3}" 
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[0.0 , -4.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[0.0 , -8.0 , 0.0]' '[0.0 , 0.0 , -6.28]'


